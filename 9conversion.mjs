// conversion to string

import { stringify } from "querystring"

/* let num = 1234
let strNum = String(num) // "1234"
console.log(strNum)
 */


//Another method
console.log(String(1234))

console.log(Number("1234"))


//conversion to boolean
console.log(Boolean("HEllo")) 
console.log(Boolean("a")) 
console.log(Boolean("0")) 
console.log(Boolean(" ")) 
console.log(Boolean("")) //false 
console.log(Boolean(0)) //false
console.log(Boolean(1)) 

/* all empty are falsy value 
string 
"" = falsy
all are truthy 

number 
0 = falsy 
all are truthy */