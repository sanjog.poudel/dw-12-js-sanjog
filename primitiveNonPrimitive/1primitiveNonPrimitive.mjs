/*
 In case of primitive 
      a new memory space is created if let keyword is used 


      let a = 1
      let b = a
      let c = 1
      a = 10
      console.log(a) /a=10
      console.log(b) /b=1
      console.log(c) /c=1
      ()===0 
      In primitive === produce true if the value are same 

In case of non primitive
      a new memory space is created if a variable is not copy of another variable 
      If a variable is copy of another variable, the variable share the memory 

    let a = [1,2]
    let b = a
    let c = [1,2]
    a.push(10)
    console.log(a) /a=[1,2,3]
    console.log(b) /b=[1,2,3]
    console.log(c) /c=[1,2]

      In non primitive === produce true if they share the same memory location. 
      */
     

    