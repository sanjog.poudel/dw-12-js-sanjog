let currentDateTime = new Date()
console.log(currentDateTime)//yyyy-mm-ddThh:mm:ss//it is called iso format

// valid iso format
//2024/02/04T06:03:02

// invalid format
//2024/2/04T6:03:02 // here 2 must be 02 and 6 must be 06