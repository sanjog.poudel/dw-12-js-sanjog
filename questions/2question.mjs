let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a1",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a2",
    },
  },
];
// find the array of id ie  output must be [1,2,3]

let ids = products.map((value, i) => {
  return value.id;
});
console.log(ids);

// find the array of title ie output must be ["Product 1", "Product 2", "Product 3"]
let _titles = products.map((value, i) => {
  return value.title;
});
console.log(_titles);

// find the array of category
let categories = products.map((value, i) => {
  return value.category;
});
console.log(categories);

// find the array of type

let types = products.map((value, i) => {
  return value.discount.type;
});
console.log(types);

// find the array of price where each price is multiplied by 3  output must be [ 15000,6000,9000]

// let prices = products.map((value,i)=>{
//     return value.price*3
// })
// console.log(prices)

// find those array of  title whose price is >= 3000=>["product 1",product 3]
let pricesGreaterThan = products.filter((value, i) => {
  if (value.price >= 3000) return true;
});
console.log(pricesGreaterThan);

// find those array of  title whose price is >= 3000=>["product 1",product 3]
// if filter and map are used simultaneously , (always use filter first)
let product3000 = products
  .filter((value, i) => {
    if (value.price >= 3000) return true;
  })
  .map((value, i) => {
    return value.title;
  });
console.log(product3000);

//find those array of title whose price does not equal to 5000 ==> ["product 2","product 3"]
let productLessThan5000 = products
  .filter((value, i) => {
    if (value.price !== 5000) return true;
  })
  .map((value, i) => {
    return value.title;
  });
console.log(productLessThan5000);

// find those array of category whose price equal to 3000 ====> ["electronics"]
let priceEqual3000 = products
  .filter((value, i) => {
    if (value.price === 3000) return true;
  })
  .map((value, i) => {
    return value.category;
  });
console.log(priceEqual3000);

let input = [
    {name:"nitan",age:29},
    {name:"ram",age:30},
    {name:"roshan",age:31} 
]


//map is used to modify element of input
// where as filter is used to filter element of input
// where as filter is used to filter element of input