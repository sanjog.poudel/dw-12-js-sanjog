/*
 () parenthesis
{} curly braces / block 
[] array 
*/
// if block get executed then the condition is true 

if (true){
    console.log("hello i am if")
}

/* let name = "nitan"
if (name === "nitan"){
    console.log("i am nitan")  i am nittan is printed cause name declare is true 
}
 */

let name = "ram"
if (name === "nitan"){
    console.log("i am nitan")
}

let a = "0"

if (a){
    console.log("hello")
}