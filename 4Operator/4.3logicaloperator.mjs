// &&, ||, !
// && true if all are true
// || true if one is true 


console.log(true&&true&&true) //true
console.log(true&&true&&false) // false
console.log(false||false||true) //true
console.log(false||false||false) //false
console.log(!true) //false
console.log(!false) //true

