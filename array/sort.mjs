/* ascending a,b,c,d
descending d,c,c,a
 */

let ar1= ['a','b','c']

//ascending sort
let ar2 = ar1.sort()
console.log(ar2) 

/* 
ascending sort 
[1,9] => [1,9]
['c', 'a'] => ['a','c']
[9,10] => [10,9]  (interview question)
['a','b','A'] => ['A','a','b'] (interview question)
[1,'a']   =>no need to note
['@','a'] =>no need to note
*/


