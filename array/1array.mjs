let names = [`sanjog`, `ram`,`shyam` , 25 ]
//              0  ,     1    ,  2

//array is used to store data of different type or same type
//retrieving all elements
console.log(names)     //['sanjog', 'ram', 'shyam' ]

//retrieving specific elements
console.log(names[0]) //sanjog 
console.log(names[1]) //ram
console.log(names[2]) //shyam

//changing elements of array
names[1] = 'hapi'
console.log(names)
console.log(names[1])
