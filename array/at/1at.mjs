let ar1 = ['a','b','c','d','e','f']
//          0   1   2    3   4    5
//         -6   -5  -4   -3  -2   -1

console.log(ar1[1])
//console.log(ar1[-2])//it is not possible

// console.log(ar1.at(1))
// console.log(ar1.at(-2))


console.log(ar1.at(-1))

//at method is used to find last element of array 