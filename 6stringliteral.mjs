let name = "Sanjog"
let surname = "Poudel"

let fullName = `my name is ${name} and surname is ${surname}`

console.log(fullName)

/* string can be define using '' , "" , ``
we can call variable inside `` using ${} but is not possible in '' , ""  
we can not use ' inside " and so on 
*/