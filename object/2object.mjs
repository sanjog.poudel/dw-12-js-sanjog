let info = {
  name: "sanjog",
  age: 20,
  isMarried: false,
};
//['name','age','isMarried'] key
//['sanjog',20,false]
//[['name','nitan'],['age',20],['isMarried',false]]

let keysArray = Object.keys(info);
console.log(keysArray);

let valuesArray = Object.values(info);
console.log(valuesArray);

let propertiesArray = Object.entries(info);
console.log(propertiesArray);
